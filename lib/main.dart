import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:untitled/photos_response.dart';
import 'package:image_picker/image_picker.dart';

Future<PhotosResponse> getData() async {
  final response = await http.get(Uri.http("192.168.1.3:4000", "photos"));
  if (response.statusCode == 200) {
    return fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load post');
  }
}

PhotosResponse fromJson(dynamic json) {
  List<Children> photos = [];
  json['children'].forEach((jsonChildren) {
    Children photo = new Children(
        name: 'http://192.168.1.3:4000/photo/' + jsonChildren['name']);
    photos.add(photo);
  });

  return new PhotosResponse(children: photos);
}

void main() => runApp(MyApp(response: getData()));


class MyApp extends StatelessWidget {
  final Future<PhotosResponse> response;

  MyApp({required this.response});

  Future getImage() async {
    final pickedCamera = await ImagePicker().getImage(source: ImageSource.camera);
    final pickedGallery = await ImagePicker().getImage(source: ImageSource.gallery);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Photos'),
        ),
        body: Center(
          child: FutureBuilder<PhotosResponse>(
              future: response,
              builder: (context, fetchData) {
                if (fetchData.hasData) {
                  return GridView.builder(
                    itemCount: fetchData.data!.children!.length,
                    itemBuilder: (context, index) {
                      final photo = fetchData.data!.children![index];
                      return GridTile(
                        child: new Container(
                            decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(photo.name!)))),
                      );
                    },
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 8.0,
                    ),
                  );
                } else if (fetchData.hasError) {
                  return Text("${fetchData.error}");
                }
                return CircularProgressIndicator();
              }),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: getImage,
          child: const Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: new BottomAppBar(
          color: Colors.white,
          child: new Row(),
        ),
      ),
    );
  }
}
