class PhotosResponse {
  List<Children>? _children;

  List<Children>? get children => _children;

  PhotosResponse({List<Children>? children}) {
    _children = children;
  }

  PhotosResponse.fromJson(dynamic json) {
    if (json["children"] != null) {
      _children = [];
      json["children"].forEach((v) {
        _children?.add(Children.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_children != null) {
      map["children"] = _children?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Children {
  String? _name;

  String? get name => _name;

  Children({String? name}) {
    _name = name;
  }

  Children.fromJson(dynamic json) {
    _name = json["name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    return map;
  }
}
